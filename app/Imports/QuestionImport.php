<?php

namespace App\Imports;

use App\Models\Question;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class QuestionImport implements ToModel, WithHeadingRow
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    private $topicId;

    public function __construct($topicId)
    {
        $this->topicId = $topicId;
    }

    public function headingRow(): int
    {
        return 1;
    }
    public function model(array $row)
    {
        // dd($row);
        $row['topic_id'] = $this->topicId;
        return new Question([
            'no'=>$row['no'],
            'title'=>$row['title'],
            'word' => $row['new_words'],
            'pronunciation_word' => $row['pronunciation'],
            'meaning' => $row['meaning'],
            'chinese_example' => $row['chinese_example'],
            'pinyin' => $row['pinyin'],
            'meaning_in_vietnamese' => $row['meaning_in_vietnamese'],
            'incorrect_answers' => $row['incorrect_answers'],
            'topic_id' => $row['topic_id'],
        ]);
    }
}
