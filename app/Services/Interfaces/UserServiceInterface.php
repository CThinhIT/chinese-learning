<?php

namespace App\Services\Interfaces;

use Illuminate\Http\Request;

interface UserServiceInterface
{
    public function register($request);

    // public function getUserProfile(Request $request);

    // public function updateProfile($request);

    // public function uploadAvatar(Request $request);

    // public function updateCard($request);

    // public function updateCredential($request);

    // public function verifyContact($request);

    // public function sendMessage($request);

    public function forgot($user);

    public function logout($request);

    public function changePassword($request);

}
