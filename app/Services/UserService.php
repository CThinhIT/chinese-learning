<?php

namespace App\Services;

use App\Constants\Constants;
use App\Entities\Address;
use App\Entities\File;
use App\Entities\Friend;
use App\Helpers\JodJob;
use App\Helpers\ModelSetHiddenHelper;
use App\Helpers\StringHandle;
use App\Models\User;
use App\Repositories\BadgesRepository;
use App\Repositories\FileRepository;
use App\Services\Interfaces\UserServiceInterface;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class UserService implements UserServiceInterface
{
    public function forgot($user)
    {
    }
    public function logout($request)
    {
    }
    public function register($request)
    {
        DB::beginTransaction();
        try {
            $user = new User();
            $user->first_name = $request->input('first_name');
            $user->last_name = $request->input('last_name');
            $user->contact_number = $request->input('contact_number');
            $user->email = $request->input('email');
            $user->user_type = 1;
            $user->status = 1;
            $user->password = bcrypt($request->input('password'));
            $user->save();
            DB::commit();
            return [
                'status' => 200,
                'data' => $user
            ];
        } catch (Exception $e) {
            Log::error('Error func register, ' . $e->getMessage());
            DB::rollback();
            return [
                'status' => 400,
                'data' => []
            ];
        }
    }

    public function getUserProfile(Request $request)
    {
        // $user = $this->userRepo->profile();
        // return $user;
    }

    public function updateProfile($request)
    {
        // $idUserCurrent = $this->getUserCurrent()->id;
        // $user = User::find($idUserCurrent);
        // $user->first_name = $request->input('first_name');
        // $user->last_name = $request->input('last_name');
        // $user->contact_number = $this->helper->getPhoneNumber($request->input('contact_number'));
        // $user->email = $request->input('email');
        // $user->date_of_birth = $request->input('date_of_birth');
        // $user->gender = $request->input('gender');
        // if ($request->filled('address')) {
        //     $address = JodJob::getLongitudeLatitudeByAddress(optional($request)->address);
        //     Address::updateOrCreate(
        //         ['user_id' => $user->id],
        //         [
        //             'name' => $request->address,
        //             'latitude' => $address['latitude'],
        //             'longitude' => $address['longitude']
        //         ]
        //     );
        // }
        // app(\App\JodStoreService::class)->updateIdentity($request, $user);
        // app(\App\JodStoreService::class)->updateFoodHygieneCertificate($request, $user);
        // $user->education_id = empty($request->education_id) ? NULL : $request->education_id;
        // $user->updated_by = $user->id;
        // $user->save();

        // return $user;
    }

    public function uploadAvatar($updateData)
    {
        // $user = $this->getUserCurrent();

        // if (!empty($user->avatar)) {
        //     Storage::delete('applicants/' . $user->avatar);
        // }
        // $storagePath = Storage::putFile('applicants', $updateData['avatar']);
        // $user->avatar = basename($storagePath);
        // $user->updated_by = $user->id;
        // $user->save();
        // $data = [
        //     'avata_url' => $user->avatar_url
        // ];

        // return $data;
    }



    public function changePassword($request)
    {
        // $user = $this->getUserCurrent();
        // $user->update([
        //     'password' => bcrypt($request->input('new_password')),
        //     'updated_by' => $user->id
        // ]);
    }

    public function forgotPassword($credentials)
    {
        // $user = $this->userRepo->userByCredentials($credentials);
        // $reset_password_token = Str::random(60);
        // $user->reset_password_token = $reset_password_token;
        // $user->save();

        // return $user;
    }

    public function updateUserProfile($updateData)
    {
        // $userId = $this->getUserCurrent()->id;
        // $user = $this->userRepo->where('id', $userId)->first();
        // $userAvatar = $user->avatar;
        // if (array_key_exists('avatar', $updateData)) {
        //     if (!empty($userAvatar)) {
        //         Storage::delete('applicants/' . $userAvatar);
        //     }
        //     $storagePath = Storage::putFile('applicants', $updateData['avatar']);
        //     $userAvatar = basename($storagePath);
        // }
        // $user->avatar = $userAvatar;
        // $user->first_name = StringHandle::removeExtraCharacter($updateData['first_name'], ' ');
        // $user->last_name = StringHandle::removeExtraCharacter($updateData['last_name'], ' ');
        // $user->job_title = StringHandle::removeExtraCharacter($updateData['job_title'], ' ');
        // $user->contact_number = $this->helper->getPhoneNumber($updateData['contact_number']);
        // if (isset($updateData['email']) && in_array($user->user_type, ['HQ', 'SUPER_ADMIN'])) {
        //     $user->email = $updateData['email'];
        // }
        // $user->language_code = $updateData['language_code'];
        // $user->save();
        // App::setLocale($user->language_code);

        // $data = [
        //     'avata_url' => $user->avatar_url
        // ];

        // return $data;
    }
}
