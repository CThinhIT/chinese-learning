<?php

namespace App\Http\Controllers;

use App\Models\Topic;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class TopicController extends Controller
{
    public function index()
    {
        $topics = Topic::all();
        return view('pages.topic.index', compact('topics'));
    }

    public function store()
    {
        return view('pages.topic.create');
    }

    public function delete($id)
    {
        DB::beginTransaction();
        try {
            $topic = Topic::query();
            $topic->where('id', '=', $id)->delete();
            DB::commit();
        } catch (Exception $e) {
            Log::error('Error func delete, ' . $e->getMessage());
            DB::rollback();
            return [
                'status' => 400,
            ];
        }
        return redirect('/topic');
    }
    
    public function update($id)
    {
        $topic = Topic::where('id', '=', $id)->first();
        return view('pages.topic.edit',compact('topic'));
    }
    public function updatePost(Request $request)
    {
        $topic = Topic::where('id', '=', $request->input('id'))->first();
        DB::beginTransaction();
        try {
            $topic->word = $request->input('name');
            $topic->save();
            DB::commit();
        } catch (\Exception $e) {
            Log::error('Error func update , ' . $e);
            DB::rollback();
        }
        return view('pages.topic.edit', compact('topic'));
    }
}
