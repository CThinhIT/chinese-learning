<?php

namespace App\Http\Controllers;

use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class UserController extends Controller
{
    public function index()
    {
        $users = User::all();
        return view('pages.user.index', compact('users'));
    }

    public function store()
    {
        return view('pages.user.create');
    }

    public function delete($id)
    {
        DB::beginTransaction();
        try {
            $users = User::query();
            $users->where('id', '=', $id)->delete();
            DB::commit();
        } catch (Exception $e) {
            Log::error('Error func delete, ' . $e->getMessage());
            DB::rollback();
            return [
                'status' => 400,
            ];
        }
        return redirect('/user');
    }
    public function update($id)
    {
        $users = User::where('id', '=', $id)->first();
        return view('pages.user.edit', compact('users'));
    }
    public function updatePost(Request $request)
    {
        $users = User::where('id', '=', $request->input('id'))->first();
        DB::beginTransaction();
        try {
            $users->email = $request->email;
            $users->first_name = $request->fName;
            $users->last_name = $request->lName;
            $users->save();
            DB::commit();
        } catch (\Exception $e) {
            Log::error('Error func update , ' . $e);
            DB::rollback();
        }
        return view('pages.user.edit', compact('users'));
    }
}
