<?php

namespace App\Http\Controllers;

use App\Exports\QuestionExport;
use App\Helpers\UploadFile;
use App\Http\Requests\QuestionImportRequest;
use App\Imports\QuestionImport;
use App\Models\Question;
use App\Models\Topic;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;

class QuestionController extends Controller
{
    public function index(Request $request)
    {
        $topics = Topic::all();
        $min =  Topic::query()->min('id');
        session()->put('topic_id', $min);
        $data = Question::query();
        if ($topics->isEmpty()) {
            $request->session()->put('topic_id', 0);
        }
        $questions = $data->where('topic_id', $min)->paginate(20);
        return view('pages.question.index', compact('topics'), compact('questions'));
    }

    public function filter(Request $request)
    {
        $topics = Topic::all();
        $input = $request->input('topic_id');
        $data = Question::query();
        if ($input == 0) {
            $questions = $data->paginate(20);
        } else {
            $questions = $data->where('topic_id', $input)->paginate(20);
        }

        $request->session()->put('topic_id', $input);
        return view('pages.question.index', compact('topics'), compact('questions'));
    }
    public function viewCreateQuestion()
    {
        $topics = Topic::all();
        return view('pages.question.create', compact('topics'));
    }
    public function store(Request $request)
    {
        $max =  Question::query()->max('no') + 1;
        DB::beginTransaction();
        try {
            $question = new Question();
            $question->no = $max;
            $question->word = $request->input('word');
            $question->topic_id = $request->input('topic_id');
            $question->title = $request->input('title');
            $question->pronunciation_word = $request->input('pronunciationWord');
            $question->meaning = $request->input('meaning');
            $question->chinese_example = $request->input('chinese_example');
            $question->pinyin = $request->input('pinyin');
            $question->meaning_in_vietnamese = $request->input('meaning_in_vietnamese');
            $question->incorrect_answers = json_encode($request->input('incorrectAnswers'));
            $question->save();
            DB::commit();
        } catch (Exception $e) {
            Log::error('Error func store, ' . $e->getMessage());
            DB::rollback();
            return [
                'status' => 400,
                'data' => []
            ];
        }
        return redirect('/');
    }

    public function export()
    {
        $topic = session()->get('topic_id') ?? Question::query()->min('no');
        $fileName = UploadFile::createFileName('Chinese_data', 'xlsx');
        $data = Question::where('topic_id', $topic)->get();
        return Excel::download(
            new QuestionExport($data),
            $fileName,
            \Maatwebsite\Excel\Excel::XLSX,
            ['Content-Type' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet']
        );
    }
    public function importFull(QuestionImportRequest $request)
    {
        $question = Question::query();
        $topicId = $request->input('topic_id');
        DB::beginTransaction();
        try {
            if ($request->hasFile('file')) {
                $question->where('topic_id', '=', $topicId)->delete();
                $import = new QuestionImport($topicId);
                Excel::import($import, $request->file('file'), storage_path('app/excel_imports'), \Maatwebsite\Excel\Excel::XLSX);
                DB::commit();
                return redirect('/filter/question?topic_id=' . $topicId)->with('success', 'Import thành công!');
            } else {
                return redirect('/filter/question?topic_id=' . $topicId)->with('error', 'Không tìm thấy file để import!');
            }
        } catch (\Exception $e) {
            Log::error('Error func import, ' . $e->getMessage());
            DB::rollback();
            return redirect('/filter/question?topic_id=' . $topicId)->with('error', 'Lỗi trong quá trình import!');
        }
    }

    public function import(QuestionImportRequest $request)
    {
        $topicId = $request->input('topic_id');
        try {
            if ($request->hasFile('file')) {
                $import = new QuestionImport($topicId);
                Excel::import($import, $request->file('file'), storage_path('app/excel_imports'), \Maatwebsite\Excel\Excel::XLSX);
                return redirect('/filter/question?topic_id=' . $topicId)->with('success', 'Import thành công!');
            } else {
                return redirect('/filter/question?topic_id=' . $topicId)->with('error', 'Không tìm thấy file để import!');
            }
        } catch (\Exception $e) {
            Log::error('Error func import, ' . $e->getMessage());
            return redirect('/filter/question?topic_id=' . $topicId)->with('error', 'Lỗi trong quá trình import!');
        }
    }

    public function addQuestion()
    {
        $topics = Topic::all();
        return view('pages.import_question', compact('topics'));
    }

    public function createTopic(Request $request)
    {
        DB::beginTransaction();
        try {
            $topic = new Topic();
            $topic->name = $request->input('name');
            $topic->save();
            DB::commit();
        } catch (Exception $e) {
            Log::error('Error func store, ' . $e->getMessage());
            DB::rollback();
            return [
                'status' => 400,
                'data' => []
            ];
        }
        return redirect('/');
    }

    public function delete($id)
    {
        DB::beginTransaction();
        try {
            $question = Question::query();
            $question->where('id', '=', $id)->delete();
            DB::commit();
        } catch (Exception $e) {
            Log::error('Error func delete, ' . $e->getMessage());
            DB::rollback();
            return [
                'status' => 400,
            ];
        }
        return redirect('/');
    }
    public function update($id)
    {
        $question = Question::where('id', '=', $id)->first();
        return view('pages.question.edit', compact('question'));
    }
    public function updatePost(Request $request)
    {
        $question = Question::where('id', '=', $request->input('id'))->first();
        DB::beginTransaction();
        try {
            $question->word = $request->input('word');
            $question->topic_id = $request->input('topic_id');
            $question->title = $request->input('title');
            $question->pronunciation_word = $request->input('pronunciation_word');
            $question->meaning = $request->input('meaning');
            $question->chinese_example = $request->input('chinese_example');
            $question->pinyin = $request->input('pinyin');
            $question->meaning_in_vietnamese = $request->input('meaning_in_vietnamese');
            $question->incorrect_answers = json_encode($request->input('incorrect_answers'));
            $question->save();
            DB::commit();
        } catch (\Exception $e) {
            Log::error('Error func update , ' . $e);
            DB::rollback();
        }
        return view('pages.question.edit', compact('question'));
    }
}
