<?php

namespace App\Http\Controllers\Apis;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    /**
     * @OA\Post(
     *   path="auth/login",
     *   summary="API Login ",
     *   description="Login",
     *   security={ {"token": {}} },
     *   operationId="login",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *            mediaType="multipart/form-data",
     *            @OA\Schema(
     *               type="object",
     *               required={"email","password"},
     *               @OA\Property(property="password",description="Job title",type="string",maxLength=255),
     *               @OA\Property(property="email",description="Email",type="string",format="email"),
     *               )
     *        )
     *    ),
     *   @OA\Response(response=200, description="Successful operation", @OA\JsonContent()),
     *   @OA\Response(response=400, description="Bad request", @OA\JsonContent()),
     *   @OA\Response(response=401, description="Unauthorized", @OA\JsonContent()),
     *   @OA\Response(response=403, description="Forbidden", @OA\JsonContent()),
     *   @OA\Response(response=404, description="Resource Not Found", @OA\JsonContent()),
     *   @OA\Response(response=500, description="Internal Server Error", @OA\JsonContent()),
     *   )
     * )
     */
    public function login(LoginRequest $request)
    {
        $credentials = [
            'email' => $request->email,
            'password' => $request->password,
        ];
        $user = User::where('email', $request->email)->first();
        if (!isset($user)) {
            return response()->json([
                // ('Tài khoản không tồn tại'),
                'message' => __('messages.users.errors.account_dont_exist'),
                'status' => 401,
            ]);
        }
        $check = Hash::check($request->password,  $user->password);
        $tokenResult = $user->createToken('authToken')->accessToken->token;
        if ($check) {
            $user->password = bcrypt($request->password);
            $user->save();
            return response()->json([
                'access_token' => $tokenResult,
                'token_type' => 'Bearer',
                'user' => $user,
                'status' => 200,
                'remember' => $request->remember_me,
            ]);
        }
        return response()->json([
            'message' => ('Sai mật khẩu'),
            'status' => 401,
        ]);
    }
}
