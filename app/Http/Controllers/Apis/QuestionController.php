<?php

namespace App\Http\Controllers\Apis;

use App\Http\Controllers\Controller;
use App\Models\Question;

class QuestionController extends Controller
{
    /**
     * Listing company option.
     *
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\Get(
     *   path="/question/list-questions",
     *   summary="API list of questions",
     *   description="List questions",
     *   operationId="listquestionss",
     *   security={ {"token": {}} },
     *   @OA\Response(response=200, description="Successful operation", @OA\JsonContent()),
     *   @OA\Response(response=400, description="Bad request", @OA\JsonContent()),
     *   @OA\Response(response=401, description="Unauthorized", @OA\JsonContent()),
     *   @OA\Response(response=403, description="Forbidden", @OA\JsonContent()),
     *   @OA\Response(response=404, description="Resource Not Found", @OA\JsonContent()),
     *   @OA\Response(response=500, description="Internal Server Error", @OA\JsonContent()),
     *   )
     * )
     */
    public function listQuestion()
    {
        $query = Question::query()->orderBy('id', 'asc')->get();

        $questions = $query->map(function ($items) {
            $data['id'] = $items->id;
            $data['no'] = $items->no;
            $data['title'] = $items->title;
            $data['topic_id'] = $items->topic_id;
            $data['pronunciation_word'] = $items->pronunciation_word;
            $data['meaning'] = $items->meaning;
            $data['chinese_example'] = $items->chinese_example;
            $data['pinyin'] = $items->pinyin;
            $data['meaning_in_vietnamese'] = $items->meaning_in_vietnamese;
            $data['incorrect_answers'] = $items->incorrect_answers;
            return $data;
        });

        return response()->json([
            'data' => $questions,
            'message' => __('messages.topics.success.index')
        ], 200);
    }
}
