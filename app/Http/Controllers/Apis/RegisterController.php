<?php

namespace App\Http\Controllers\Apis;

use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterRequest;
use App\Services\Interfaces\UserServiceInterface;

class RegisterController extends Controller
{
    protected $userService;
    public function __construct(
        UserServiceInterface $userService,
    ) {
        $this->userService = $userService;
    }

    /**
     * @OA\Post(
     *   path="/sign-up",
     *   summary="API create user ",
     *   description="Create user ",
     *   security={ {"token": {}} },
     *   operationId="createUser",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *            mediaType="multipart/form-data",
     *            @OA\Schema(
     *               type="object",
     *               required={"first_name","last_name","contact_number","email","password","repassword","user_type","status"},
     *               @OA\Property(property="first_name",description="First name",type="string",maxLength=255),
     *               @OA\Property(property="last_name",description="Last name",type="string",maxLength=255),
     *               @OA\Property(property="password",description="Job title",type="string",maxLength=255),
     *               @OA\Property(property="contact_number",description="Contact number",type="string",minLength=8,maxLength=8),
     *               @OA\Property(property="email",description="Email",type="string",format="email"),
     *               @OA\Property(property="repassword",description="Reqassword",type="string",maxLength=255),
     *               @OA\Property(property="user_type",description="User Type",type="string",maxLength=255),
     *               @OA\Property(property="status",description="Status",type="string",maxLength=255),
     *            )
     *        )
     *    ),
     *   @OA\Response(response=200, description="Successful operation", @OA\JsonContent()),
     *   @OA\Response(response=400, description="Bad request", @OA\JsonContent()),
     *   @OA\Response(response=401, description="Unauthorized", @OA\JsonContent()),
     *   @OA\Response(response=403, description="Forbidden", @OA\JsonContent()),
     *   @OA\Response(response=404, description="Resource Not Found", @OA\JsonContent()),
     *   @OA\Response(response=500, description="Internal Server Error", @OA\JsonContent()),
     *   )
     * )
     */
    public function register(RegisterRequest $request)
    {
        $result = $this->userService->register($request);

        if ($result['status'] == 400) {
            return response()->json([
                'message' => ('Lỗi khi tạo tài khoản')
            ], 400);
        }
        $user = $result['data'];
        return response()->json([
            'data' => $user,
            'message' => ('Đăng ký thành công'),
            'status' => 200
        ], 200);
    }
}
