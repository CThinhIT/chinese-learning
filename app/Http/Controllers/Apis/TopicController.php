<?php

namespace App\Http\Controllers\Apis;

use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterRequest;
use App\Models\Topic;
use App\Services\Interfaces\UserServiceInterface;

class TopicController extends Controller
{
    /**
     * Listing company option.
     *
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\Get(
     *   path="/topic/list-topics",
     *   summary="API list of topic",
     *   description="List topics",
     *   operationId="listTopics",
     *   security={ {"token": {}} },
     *   @OA\Response(response=200, description="Successful operation", @OA\JsonContent()),
     *   @OA\Response(response=400, description="Bad request", @OA\JsonContent()),
     *   @OA\Response(response=401, description="Unauthorized", @OA\JsonContent()),
     *   @OA\Response(response=403, description="Forbidden", @OA\JsonContent()),
     *   @OA\Response(response=404, description="Resource Not Found", @OA\JsonContent()),
     *   @OA\Response(response=500, description="Internal Server Error", @OA\JsonContent()),
     *   )
     * )
     */
    public function listTopic()
    {
        $query = Topic::query()->orderBy('name', 'asc')->get();

        $topics = $query->map(function ($items) {
            $data['id'] = $items->id;
            $data['name'] = $items->name;
            return $data;
        });

        return response()->json([
            'data' => $topics,
            'message' => __('messages.topics.success.index')
        ], 200);
    }
}
