<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

/**
 * @OA\Info(
 *      version="1.0.7",
 *      title="Jod API",
 *      description="Swagger OpenApi of Jod",
 *      @OA\Contact(
 *          email="luat.vu@kyanon.digital"
 *      ),
 *     @OA\License(
 *         name="Apache 2.4",
 *         url="http://www.apache.org/licenses/LICENSE-2.0.html"
 *     )
 * )
 * @OA\Server(
 *      url=L5_SWAGGER_CONST_HOST,
 *      description="API JOD"
 * ),
 * @OA\SecurityScheme(
 *     type="apiKey",
 *     description="Using bearer token. Example: Bearer [token]",
 *     in="header",
 *     name="authorization",
 *     securityScheme="token",
 * )
 */
class Controller extends BaseController
{

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}
