<?php

namespace App\Exports;

use App\Models\Question;
use Illuminate\Database\Eloquent\Collection;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Events\AfterSheet;

class QuestionExport implements FromCollection, WithHeadings, WithEvents, WithMapping
{
    /**
     * @return \Illuminate\Support\Collection
     */
    protected $data;

    public function __construct(Collection $data)
    {
        $this->data = $data;
    }
    public function collection()
    {
        return $this->data;
    }

    public function headings(): array
    {
        return [
            [
                __('No'),
                __('Title'),
                __('New Words'),
                __('Pronunciation'),
                __('Meaning'),
                __('Chinese Example'),
                __('Pinyin'),
                __('Meaning in Vietnamese'),
                __('Incorrect Answers'),
            ]
        ];
    }
    public function map($row): array
    {
        return [
            $row['no'],
            $row['title'],
            $row['word'],
            $row['pronunciation_word'],
            $row['meaning'],
            $row['chinese_example'],
            $row['pinyin'],
            $row['meaning_in_vietnamese'],
            $row['incorrect_answers'],
        ];
    }
    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $event->sheet->getStyle('A1:I1')->getAlignment()->setHorizontal('center');
                $event->sheet->getStyle('A:I')
                    ->getFont()
                    ->applyFromArray([
                        'size' => 12,
                    ]);

                $event->sheet->getColumnDimension('A')->setWidth(10);
                $event->sheet->getColumnDimension('B')->setWidth(20);
                $event->sheet->getColumnDimension('C')->setWidth(15);
                $event->sheet->getColumnDimension('D')->setWidth(25);
                $event->sheet->getColumnDimension('E')->setWidth(35);
                $event->sheet->getColumnDimension('F')->setWidth(35);
                $event->sheet->getColumnDimension('G')->setWidth(40);
                $event->sheet->getColumnDimension('H')->setWidth(50);
                $event->sheet->getColumnDimension('I')->setWidth(50);
                $event->sheet->getDelegate()->getStyle('A1:I1')
                    ->getFill()
                    ->applyFromArray(['fillType' => 'solid', 'rotation' => 0, 'color' => ['rgb' => '00005b']]);
                $event->sheet->getDelegate()->getStyle('A1:I1')
                    ->getFont()
                    ->applyFromArray([
                        'bold' => TRUE,
                        'color' => ['rgb' => 'FFFFFF']
                    ]);

                // $event->sheet->getDelegate()->getStyle('A3:I3')
                //     ->getAlignment()
                //     ->applyFromArray([
                //         'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                //         'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER
                //     ]);

                // $event->sheet->getDelegate()->getStyle('A3:H3')
                //     ->getFont()
                //     ->applyFromArray([
                //         'bold' => TRUE,
                //     ]);
            },
        ];
    }
}
