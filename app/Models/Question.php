<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    use HasFactory;
    protected $table = 'question';
    protected $fillable = [
        'no',
        'word',
        'title',
        'topic_id',
        'pronunciation_word',
        'meaning',
        'chinese_example',
        'pinyin',
        'meaning_in_vietnamese',
        'incorrect_answers'
    ];
    protected $casts = [
        'incorrect_answers' => 'array'
    ];
    public function topic()
    {
        return $this->belongsTo(Topic::class);
    }
}
