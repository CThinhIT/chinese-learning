<?php

namespace App\Helpers;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class UploadFile
{
    /**
     * @param UploadedFile $file
     * @param string $folder
     * @param string $fileName
     * @return string|false
     */
    public static function uploadFile(UploadedFile $file, $folder, $fileName)
    {
        $path = $file->storeAs($folder, $fileName);
        return $path;
    }

    public static function createFileName($name, $extension)
    {
        $current = time();
        $fileName = $name . '_' . $current . '.' . $extension;
        return $fileName;
    }

    public static function getAssetUrl($path = null)
    {
        if (is_null($path)) {
            return asset(Storage::url('/'));
        }

        return asset(Storage::url($path));
    }
}
