<?php

namespace App\Traits;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

trait FailedValidationException
{
    public function failedValidation(Validator $validator)
    {
        //write your bussiness logic here otherwise it will give same old JSON response
        throw new HttpResponseException(
            response()->json([
                'error' => [
                    'message' => ('validation_failed'),
                    'errorData' =>  $validator->errors()
                ]
            ], 400)
        );
    }
}
