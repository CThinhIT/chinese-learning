<?php

namespace App\Repositories;

use App\Constants\Constants;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\UserRepository;
use App\Models\User;
use App\Helpers\StringHandle;
use App\Traits\ConfigurationTrait;
use Illuminate\Support\Facades\DB;
use App\Traits\UserInfoCurrent;
use Illuminate\Support\Facades\Hash;

/**
 * Class UserRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class UserRepositoryEloquent extends BaseRepository implements UserRepository
{
    // use UserInfoCurrent;
    // use ConfigurationTrait;

    // /**
    //  * Specify Model class name
    //  *
    //  * @return string
    //  */
    public function model()
    {
        return User::class;
    }

    public function getUser($id)
    {
        return $this->model
            // ->where('user_type', config('portal.user.user_type.user_app'))
            ->find($id);
    }

    public function getAllUser()
    {
        return $this->model
            ->get();
    }
    public function udpateProfile($request, $user)
    {
        $user->email = $request->email;
        $user->first_name = $request->fName;
        $user->last_name = $request->lName;
        $user->save();
    }
    public function changePassword($request, $user)
    {
        $check = Hash::check($request->old_password,  $user->password);
        if ($check) {
            $user->password = bcrypt($request->password);
            $user->save();
            return true;
        }
        return false;
    }
}
