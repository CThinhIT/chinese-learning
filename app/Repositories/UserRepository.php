<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface UserRepository.
 *
 * @package namespace App\Repositories;
 */
interface UserRepository extends RepositoryInterface
{
    // public function searchHqUser($dataFilter);

    // public function searchApplicantUser($dataFilter);

    // public function getListAreaLocationUser($dataFilter, $companyId);

    // public function getListManagers($dataFilter);

    // public function getListApplicantUserByHqUser($dataFilter, $companyId);

    // public function searchApplicantUserApplyJodJobByJobId($dataFilter, $jobId);

    // public function getUserInfoLoginById($userId);

    // public function findOrFail(array $option, $id);

    // public function detail($id);

    // public function userByCredentials(array $credentials);

    // public function getReferralUser($referralCode);

    // public function profile();

    // public function getInfoHqManagerOfUserByCompanyId($companyId);

    // public function getLocationUserById($id);

    // public function getPaymentHistoryChartWithRangeYearOfRatingRepo($startDate, $endDate, $applicantId);

    // public function getPaymentHistoryChartWithRangeMonthOfRatingRepo($startDate, $endDate, $year, $applicantId);

    // public function removeUserDeviceKeyByUserId($userId);

    // public function getAllAppUserDeviceKeyWithNotificationSetting();

    // public function getAllAppUserWithInAppNotificationSetting();

    // public function getAppUserWithInAppNotificationSettingByUserId($userId);

    // public function getAllAppUserDeviceKeyForOpenJobWithNotificationSetting(array $coordinates);

    // public function getAllAppUserForOpenJobWithInAppNotificationSetting();

    // public function deleteOwnApplicantAccount($attributesToEmpty, $applicantUserId);

    // public function getListUsers($filters);

    // public function storeHqUser($insertData);

    // public function updateBankInfo($id, $request);

    // public function getTotalNumberOfManagers($userType = '');

    public function getUser($id);
    public function getAllUser();
    public function udpateProfile($request, $user);
    public function changePassword($request, $user);
}
