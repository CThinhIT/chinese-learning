<?php

return [
    "auth" => [
        "errors" => [
            "account_disable" => "Your account has been disabled. Please contact :email_contact",
            "account_has_not_completed_registration" => "Click continue to verify your mobile phone number",
            "applicant_failed_login_limitation" => "Applicant failed login limitation",
            "authentication" => "Phone number or password is incorrect",
            "login_account_disable" => "Login failed! Your account is disabled",
            "manager_failed_login_limitation" => "Manager failed login limitation",
            "user_is_disabled" => "Your account has been disabled"
        ]
    ],
    "banks" => [
        "success" => [
            "index" => "Retrieved successfully"
        ]
    ],
    "topics" => [
        "success" => [
            "index" => "Retrieved successfully"
        ]
    ],
    "educations" => [
        "success" => [
            "listOption" => "Retrieved successfully"
        ]
    ],
    "invite_friends" => [
        "success" => [
            "store" => "Your friend successfully refered"
        ]
    ],
    "jobs" => [
        "errors" => [
            "api_v1_wrong_response_format" => "API V1 wrong response format",
            "applicant_apply_job_fail" => "Apply job fail",
            "applicant_can_not_ack1" => "Applicant cannot do ACK1",
            "applicant_can_not_ack2" => "Please acknowledge again within 3 hours before the job starts",
            "applicant_can_not_cancel_the_job_after_clocking_in" => "Applicant cannot cancel the job after clocking in",
            "applicant_can_not_clock_in" => "Applicant cannot clock in",
            "applicant_can_not_reject_job" => "Cannot cancel job",
            "applicant_do_not_ack2" => "Applicant has not done ACK2 yet",
            "applicant_had_done_ack1" => "Applicant has done ACK1",
            "applicant_had_done_ack2" => "Applicant has done ACK2",
            "applicant_has_apply_job" => "You have already applied for this job",
            "applicant_has_job_in_time" => "Cannot apply for time-overlapped job",
            "applicant_has_slot_need_to_feed_back" => "Applicant has slot need to feed back",
            "applicant_job_is_not_selected" => "Applicant was not selected for the job",
            "applicant_job_not_exist" => "You do not have any available job at the moment",
            "applicant_reject_job_fail" => "Applicant reject job fail",
            "applicant_slot_not_exist" => "You do not have any available slot at the moment",
            "applicant_user_not_found" => "Applicant has not been found",
            "area_user_has_not_company" => "Area manager does not belong to any company at the moment",
            "area_user_not_allow_for_location" => "Area user do not allow for location",
            "area_user_not_found" => "Area manager has not been found",
            "can_not_call_api_v1" => "Cannot call api V1",
            "created_date_is_not_valid" => "The applicant's created date is not valid",
            "error_approve_applicant" => "Selecting applicant failed",
            "error_clock_out_job" => "Clock out job fail",
            "error_create_job" => "Creating job failed",
            "error_do_ack1" => "do Ack1 failed",
            "error_do_ack2" => "do Ack2 failed",
            "error_edit_job" => "Editing job failed",
            "error_reject_applicant" => "Rejecting applicant failed",
            "hq_user_do_not_allow" => "Hq user do not allow with job",
            "hq_user_not_found" => "HQ manager has not been found",
            "invalid_identity" => "Your identity (NRIC/FIN) was verified as invalid. Please check and update it",
            "job_can_not_apply" => "Cannot apply for this job",
            "job_can_not_cancel" => "Job cannot be canceled",
            "job_can_not_clock_out_before_slot_start" => "Applicant cannot clock out before slot start time",
            "job_can_not_delete" => "Job cannot be cancelled",
            "job_can_not_edit" => "Job cannot be edited",
            "job_do_not_slot" => "The required working days must match the Start date - End date range",
            "job_has_not_clock_in_qr_code" => "Job has not clock in qr code",
            "job_has_not_clock_out_qr_code" => "Job has not clock out qr code",
            "job_is_not_active" => "Job is not Active",
            "job_is_not_open" => "This job posting is no longer available",
            "job_job_not_found" => "Job has not been found",
            "job_template_not_found" => "Job template has not been found",
            "job_type_not_found" => "Job type has not been found",
            "location_do_not_allow_location_user_create_job" => "The location does not allow Location managers to create job",
            "location_not_found" => "Location has not been found",
            "location_user_has_not_company" => "Location manager does not belong to any company at the moment",
            "location_user_have_not_location" => "Location manager does not manage any location at the moment",
            "location_user_not_allow_for_location" => "Location user do not allow for location",
            "location_user_not_found" => "Location manager has not been found",
            "not_found" => "Data has not been found",
            "slot_do_not_start" => "Slot has not started yet",
            "slot_end_date" => "Slot has ended",
            "slot_not_found" => "Slot has not been found",
            "slot_user_do_not_ack2" => "Slot applicant do not ACK2",
            "slot_user_had_done_clock_out" => "Slot user had done clock out",
            "slot_user_has_feed_back" => "Applicant has feed back",
            "slot_user_have_not_clock_out_time" => "Slot user have not clock out time",
            "slot_user_is_not_clock_in" => "Applicant's status in the slot is not Clocked In",
            "slot_user_is_not_clock_out" => "Applicant's status in the slot is not Clocked Out",
            "slot_user_is_not_status_ack2" => "Applicant's status in the slot is not ACK2",
            "slot_user_status_is_not_allow_for_ack2" => "Current applicant's status in the slot is not allowed to do ACK2",
            "unavailable_identity" => "Please provide your identity (NRIC/FIN) in your profile",
            "unverified_identity" => "Your identity (NRIC/FIN) hasn't been verified by JOD Team yet",
            "user_is_not_allow_with_job_template" => "User do not allow with job template",
            "user_not_allow_with_job" => "User not allow with job",
            "user_not_found" => "User has not been found"
        ],
        "success" => [
            "adjustmentSlotUser" => "Successfully",
            "applicantApplyJob" => "You have applied for the job successfully",
            "applicantJobDetail" => "Retrieved successfully",
            "approveApplicantSelectForJob" => "Successfully",
            "approveJobForHqUser" => "Approved job successfully",
            "cancelJodJob" => "Canceled job successfully",
            "cancelMultipleJob" => "{1} :value job has been cancelled|[2,*] :value jobs have been cancelled",
            "cancelPreJob" => "You have cancelled your job successfully",
            "confirmAdjustmentSlotUser" => "Successfully",
            "delete" => "Deleted job successfully",
            "doAck1InJob" => "You have acknowledged the job successfully",
            "doAck2InSlot" => "You have acknowledged the job successfully",
            "doClockInSlot" => "Your next job starts in 3 hours. Please acknowledge the job under your Approved Jobs",
            "doClockOutSlot" => "You have clocked out the job successfully",
            "doConfirmFormInSlot" => "You have completed your feedback successfully",
            "getChart" => "Retrieved chart history successfully",
            "getConfirmFormInSlot" => "Retrieved successfully",
            "getDashboardJobs" => "Retrieved successfully",
            "getFeedBackOfJob" => "Retrieved successfully",
            "getJobHistoryNumberTotal" => "Retrieved successfully",
            "getRejectApplicantTotal" => "Retrieved successfully",
            "getReports" => "Retrieved successfully",
            "jobDetail" => "Retrieved successfully",
            "jobHistoryDetail" => "Retrieved successfully",
            "list" => "Retrieved successfully",
            "listActiveJob" => "Retrieved successfully",
            "listApprovedJob" => "Retrieved successfully",
            "listFeedbackJob" => "Retrieved successfully",
            "listJobByCompany" => "Retrieved successfully",
            "listJobByJobtype" => "Retrieved successfully",
            "listJobByLocation" => "Retrieved successfully",
            "listJobHistory" => "Retrieved successfully",
            "listOpeningJob" => "Retrieved successfully",
            "listPendingJob" => "Retrieved successfully",
            "listUrgentJobs" => "Retrieved successfully",
            "listWishlistJob" => "Retrieved successfully",
            "paySlipRequest" => "Retrieved successfully",
            "reject" => "Reject applicant in job successfully",
            "rejectPendingJodJob" => "You have cancelled your job successfully",
            "rejectSlotInJob" => "Applicant reject slot in job success",
            "search" => "Retrieved successfully",
            "summary" => "Retrieved successfully",
            "thisMonthEarning" => "Retrieved successfully",
            "updateJobUpdating" => "{1} :job_updated out of :job_request job have been updated|[2,*] :job_updated out of :job_request jobs have been updated"
        ]
    ],
    "jobtypes" => [
        "success" => [
            "index" => "Retrieved successfully",
            "listOption" => "Retrieved successfully",
            "listOptionForSearch" => "Retrieved successfully"
        ]
    ],
    "locations" => [
        "errors" => [
            "api_v1_wrong_response_format" => "API V1 wrong response format",
            "can_not_call_api_v1" => "Cannot call api V1"
        ],
        "success" => [
            "getLocationsByCompanyId" => "Retrieved successfully",
            "index" => "Retrieved successfully",
            "listOption" => "Retrieved successfully",
            "listOptionForSearch" => "Retrieved successfully",
            "popularOutlets" => "Retrieved successfully"
        ]
    ],
    "payments" => [
        "errors" => [
            "no_payment_at_the_moment" => "No payment at the moment",
            "over_downloads_this_month" => "You have used up all your downloads this month",
            "payslip_no_data" => "You have no payments for this month",
            "request_in_processing" => "The request is being processed"
        ],
        "success" => [
            "payslipRequest" => "Retrieved successfully"
        ]
    ],
    "portal" => [
        "applicantusers" => [
            "errors" => [
                "account_dont_exist" => "Your account is not an applicant account",
                "applicant_has_active_job" => "You cannot delete this account because there is related active job in working",
                "applicant_has_active_job_or_pending_feedback" => "Cannot delete account while you don't complete all your jobs and pending feedback",
                "applicant_has_clock_in_slot" => "The applicant has :count pending clock-out job(s). Do you want to disable the applicant?",
                "applicant_is_disable" => "Applicant is disable",
                "deleteCertificate" => "Delete certificate failure",
                "disable_applicant_failure" => "Disable applicant failure",
                "not_found" => "Applicant user not found",
                "something_went_wrong" => "Something went wrong",
                "updateCertificate" => "Update certificate failure",
                "update_fail" => "Updated applicant user failure",
                "user_not_found" => "User is not found"
            ],
            "success" => [
                "applicantUserSummaryByStatus" => "Retrieved successfully",
                "deleteCertificate" => "Delete certificate successfuly",
                "deleteOwnApplicantAccount" => "Your applicant account is deleted",
                "detail" => "Retrieved successfully",
                "disable" => "Disable applicant user successfully",
                "enable" => "Enable Applicant user successfully",
                "index" => "Retrieved successfully",
                "indexSlotByAppUserId" => "Retrieved successfully",
                "show" => "Retrieved successfully",
                "store" => "Created application user successfully",
                "update" => "Updated applicant user successfully",
                "updateCertificate" => "Update certificate successfuly",
                "updateRating" => "Update rate successfully"
            ]
        ],
        "areausers" => [
            "errors" => [
                "area_manager_not_found" => "Area manager has not been found",
                "area_user_not_found" => "Area user not found",
                "hquser_has_not_comapny" => "HQ user has not company",
                "hquser_is_not_allow" => "HQ user is not allow",
                "hquser_not_exist" => "HQ user is not exist",
                "hquser_not_found" => "HQ user not found",
                "hquser_not_has_comapny" => "HQ user not found",
                "location_has_assign" => "Location has assign for other area user",
                "location_not_found" => "There is disabled location",
                "policy" => "You do not own this user"
            ],
            "success" => [
                "areaUserSummaryByStatus" => "Retrieved successfully",
                "disable" => "Disable area user successfully",
                "enable" => "Enable area user successfully",
                "getLocation" => "Retrieved successfully",
                "index" => "Retrieved successfully",
                "listAreaEnable" => "Show area user enable successfully",
                "show" => "Retrieved successfully",
                "store" => "Created area user successfully",
                "update" => "Updated area user successfully"
            ]
        ],
        "auth" => [
            "errors" => [
                "account_disable" => "Your account is disabled",
                "applicant_login_portal" => "It looks like you are trying to log in to the portal with an applicant account. Please log in via the application",
                "authentication" => "Email or password is incorrect",
                "company_is_disabled" => "You are not allowed to sign in because your company is disabled",
                "location_is_disabled" => "You are not allowed to sign in because your location is disabled",
                "login_account_disable" => "Login failed! Your account is disabled",
                "no_device_tokens" => "No device token",
                "no_location_managed_by_your_account" => "Login failed! There is no location managed by your account",
                "reset_password_token" => "The reset password key is not valid or is expired",
                "unverified_email" => "Unverified email",
                "user_not_found" => "User not found"
            ],
            "success" => [
                "forgot" => "Send mail success",
                "logout" => "User successfully signed out",
                "pushNotify" => "Push notification success",
                "register" => "User successfully registered",
                "resetPassword" => "Reset pasword success",
                "reset_password_token" => "The reset password key is valid",
                "show" => "Retrieved successfully",
                "update" => "Updated successfully"
            ]
        ],
        "badges" => [
            "success" => [
                "index" => "Retrieved successfully",
                "update" => "Updated successfully"
            ]
        ],
        "billings" => [
            "success" => [
                "index" => "Retrieved successfully"
            ]
        ],
        "companies" => [
            "errors" => [
                "cant_disable_because_related_active_job_is_working" => "Company cannot be disabled because there is related active job in working",
                "company_is_disable" => "Company is disable",
                "company_not_found" => "Company not found",
                "disable_company_failure" => "Disable company failure",
                "file_not_found" => "File not found"
            ],
            "success" => [
                "companySummaryByStatus" => "Retrieved successfully",
                "disable" => "Disable company successfully",
                "enable" => "Enable company successfully",
                "listCompanyOption" => "Retrieved successfully",
                "show" => "Retrieved successfully",
                "store" => "Created company successfully",
                "update" => "Updated company successfully"
            ]
        ],
        "configurations" => [
            "errors" => [
                "update_configuration_fail" => "Update configuration fail"
            ],
            "success" => [
                "index" => "Retrieved successfully",
                "listOfOptionsSalaries" => "Retrieved successfully",
                "update" => "Updated configuration successfully"
            ]
        ],
        "credits" => [
            "errors" => [
                "amount_is_not_enough" => "The remaining amount is not enough to assign",
                "area_has_not_been_assigned_company" => "Area user has not been assigned company",
                "company_not_found" => "Company not found",
                "disable_company" => "Company is disable",
                "hq_has_not_been_assigned_company" => "User HQ has not been assigned company"
            ],
            "success" => [
                "adminAssignCredit" => "Assigned credit successfully",
                "allCreditHistoryLocationByHq" => "Retrieved successfully",
                "availableCreditsTotalByAreaUser" => "Retrieved successfully",
                "availableCreditsTotalByHq" => "Retrieved successfully",
                "companyHistoryCredit" => "Retrieved successfully",
                "creditHistoryOfALocation" => "Retrieved successfully",
                "creditHistoryOfLocationsByAreaUser" => "Retrieved successfully",
                "creditHistoryOfLocationsByLocationUser" => "Retrieved successfully",
                "creditHistoryOfLocationsWithLocationIdByAreaUser" => "Retrieved successfully",
                "historyCredit" => "Retrieved successfully",
                "hqAssignCredit" => "Assigned credit for location successfully",
                "jodCreditAvailableHq" => "Retrieved successfully"
            ]
        ],
        "educations" => [
            "errors" => [
                "not_found" => "Education not found"
            ],
            "success" => [
                "disable" => "Disable education successfully",
                "educationSummaryByStatus" => "Retrieved successfully",
                "enable" => "Enable education successfully",
                "index" => "Retrieved successfully",
                "listEducationOption" => "Retrieved successfully",
                "search" => "Retrieved successfully",
                "show" => "Retrieved successfully",
                "store" => "Created education successfully",
                "update" => "Updated education successfully"
            ]
        ],
        "features" => [
            "success" => [
                "getListFeatures" => "Retrieved successfully"
            ]
        ],
        "hqusers" => [
            "errors" => [
                "company_has_user" => "Company has HQ user",
                "company_not_found" => "Company not found",
                "disable_company" => "Company is disable",
                "hquser_has_not_company" => "HQ user has not company",
                "hquser_is_not_allow" => "HQ user is not allow",
                "hquser_not_found" => "HQ user not found"
            ],
            "success" => [
                "disable" => "Disable HQ user successfully",
                "enable" => "Enable HQ user successfully",
                "hqUserSummaryByStatus" => "Retrieved successfully",
                "index" => "Retrieved successfully",
                "indexApplicantUserByHqUser" => "Retrieved successfully",
                "indexAreaLocationUser" => "Retrieved successfully",
                "show" => "Retrieved successfully",
                "store" => "Created HQ user successfully",
                "update" => "Updated HQ user successfully"
            ]
        ],
        "internal_users" => [
            "errors" => [
                "internaluser_not_found" => "Internal user not found",
                "role_is_not_available" => "Role :role is not available",
                "role_not_found" => "Role not found"
            ],
            "success" => [
                "disable" => "Disabled user successfully",
                "enable" => "Enabled user successfully",
                "getCurrentUserInfo" => "Retrieved successfully",
                "getInternalUserDetail" => "Retrieved successfully",
                "getListInternalUsers" => "Retrieved successfully",
                "store" => "Created Internal user successfully",
                "update" => "Updated Internal user successfully"
            ]
        ],
        "job_templates" => [
            "errors" => [
                "hquser_is_not_allow" => "HQ user is not allow",
                "hquser_not_exist" => "HQ user is not exist",
                "hquser_not_has_comapny" => "HQ user has not company",
                "job_template_not_found" => "Job template has not been found"
            ],
            "success" => [
                "disable" => "Disable job template successfully",
                "enable" => "Enable job template successfully",
                "index" => "Retrieved successfully",
                "jobTemplateSummaryByStatus" => "Retrieved successfully",
                "show" => "Retrieved successfully",
                "store" => "Created job template successfully",
                "update" => "Updated job template successfully"
            ]
        ],
        "jobs" => [
            "errors" => [
                "applicant_can_not_ack1" => "Applicant cannot do ACK1",
                "applicant_can_not_ack2" => "Please acknowledge again within 3 hours before the job starts",
                "applicant_can_not_cancel_the_job_after_clocking_in" => "Applicant cannot cancel the job after clocking in",
                "applicant_can_not_clock_in" => "Applicant cannot clock in",
                "applicant_do_not_ack2" => "Applicant has not done ACK2 yet",
                "applicant_had_done_ack1" => "Applicant has done ACK1",
                "applicant_had_done_ack2" => "Applicant has done ACK2",
                "applicant_has_apply_job" => "You have already applied for this job",
                "applicant_has_job_in_time" => "Cannot apply for time-overlapped job",
                "applicant_job_is_not_selected" => "Applicant was not selected for the job",
                "applicant_slot_not_exist" => "Applicant does not have any available slot at the moment",
                "applicant_user_not_found" => "Applicant has not been found",
                "area_user_has_not_company" => "Area manager does not belong to any company at the moment",
                "area_user_not_allow_for_location" => "Area user do not allow for location",
                "area_user_not_found" => "Area manager has not been found",
                "error_approve_applicant" => "Selecting applicant failed",
                "error_create_job" => "Creating job failed",
                "error_edit_job" => "Editing job failed",
                "error_reject_applicant" => "Rejecting applicant failed",
                "generate_clock_in_qr_code_fail" => "Generate clock in qr code fail",
                "generate_clock_out_qr_code_fail" => "Generate clock out qr code fail",
                "hq_user_do_not_allow" => "HQ user do not allow with job",
                "hq_user_not_found" => "HQ manager has not been found",
                "hquser_has_not_comapny" => "HQ user has no company",
                "hquser_is_not_allow" => "HQ user is not allowed",
                "hquser_not_found" => "HQ user not found",
                "job_can_not_apply" => "Cannot apply for this job",
                "job_can_not_clock_out_before_slot_start" => "Applicant cannot clock out before slot start time",
                "job_can_not_delete" => "Job cannot be cancelled",
                "job_can_not_edit" => "Job cannot be edited",
                "job_do_not_slot" => "The required working days must match the Start date - End date range",
                "job_is_not_active" => "Job is not active",
                "job_is_not_open" => "This job posting is no longer available",
                "job_is_not_pending" => "Job is not pending",
                "job_job_not_found" => "Job has not been found",
                "job_template_not_found" => "Job template has not been found",
                "job_type_not_found" => "Job type has not been found",
                "job_with_hourly_rate_as_current_wage_per_hour" => "Sorry, there is no job with \$:hourly_rate as current wage per hour",
                "location_do_not_allow_location_user_create_job" => "The location does not allow location managers to create job",
                "location_not_found" => "Location has not been found",
                "location_user_has_not_company" => "Location manager does not belong to any company at the moment",
                "location_user_have_not_location" => "Location manager does not manage any location at the moment",
                "location_user_not_allow_for_location" => "Location user do not allow for location",
                "location_user_not_found" => "Location manager has not been found",
                "no_area_or_location_user_is_selected" => "Creating a new job failed! There must be either area manager or location manager selected",
                "not_found" => "Jobtype not found",
                "slot_not_found" => "Slot has not been found",
                "slot_user_do_not_ack2" => "Slot applicant do not ACK2",
                "slot_user_have_not_clock_out_time" => "Slot user have not clock out time",
                "slot_user_is_not_clock_in" => "Slot user id not clock in",
                "slot_user_is_not_status_ack2" => "Applicant's status in the slot is not ACK2",
                "slot_user_status_is_not_allow_for_ack2" => "Current applicant's status in the slot is not allowed to do ACK2",
                "store" => "Created job failure",
                "user_is_not_allow_with_job_template" => "User do not allow with job template",
                "user_not_allow_with_job" => "User not allow with job",
                "user_not_found" => "User has not been found"
            ],
            "success" => [
                "adjustmentSlotUser" => "Successfully",
                "approveApplicantSelectForJob" => "Successfully",
                "approveJobForHqUser" => "Approved job successfully",
                "cancelJodJob" => "Canceled job successfully",
                "cancelMultipleJob" => "{1} :value job has been cancelled|[2,*] :value jobs have been cancelled",
                "confirmAdjustmentSlotUser" => "Successfully",
                "delete" => "Delete job opening successfully",
                "getFeedBackOfJob" => "Retrieved successfully",
                "getJobHistoryNumberTotal" => "Retrieved successfully",
                "getRejectApplicantTotal" => "Retrieved successfully",
                "getReports" => "Retrieved successfully",
                "index" => "Retrieved successfully",
                "list" => "Retrieved successfully",
                "reject" => "Reject applicant in job successfully",
                "show" => "Retrieved successfully",
                "store" => "Created job successfully",
                "summary" => "Retrieved successfully",
                "update" => "Updated job successfully",
                "updateJobUpdating" => "{1} :job_updated out of :job_request job have been updated|[2,*] :job_updated out of :job_request jobs have been updated"
            ]
        ],
        "jobtypes" => [
            "errors" => [
                "not_found" => "Job type not found"
            ],
            "success" => [
                "disable" => "Disable job type successfully",
                "enable" => "Enable job type successfully",
                "index" => "Retrieved successfully",
                "jobtypeSummaryByStatus" => "Retrieved successfully",
                "listJobTypeOption" => "Retrieved successfully",
                "listJobTypeOptionForJob" => "Retrieved successfully",
                "search" => "Retrieved successfully",
                "show" => "Retrieved successfully",
                "store" => "Created job type successfully",
                "update" => "Updated job type successfully"
            ]
        ],
        "location_users" => [
            "errors" => [
                "disable_location" => "This location is disabled",
                "hquser_has_not_comapny" => "HQ user has not company",
                "hquser_is_not_allow" => "HQ user is not allow",
                "hquser_not_exist" => "HQ user is not exist",
                "hquser_not_found" => "HQ user not found",
                "hquser_not_has_comapny" => "HQ user has not company",
                "location_has_assign" => "Location has assign for other location user",
                "location_not_found" => "Location has not been found",
                "location_user_not_found" => "Location user not found",
                "policy" => "You do not own this user"
            ],
            "success" => [
                "disable" => "Disable location user successfully",
                "enable" => "Enable location user successfully",
                "index" => "Retrieved successfully",
                "list" => "Retrieved successfully",
                "show" => "Retrieved successfully",
                "store" => "Created location user successfully",
                "summary" => "Retrieved successfully",
                "update" => "Updated location user successfully"
            ]
        ],
        "locations" => [
            "errors" => [
                "area_user_not_found" => "This manager is disabled",
                "area_user_not_valid" => "Area user not valid",
                "cant_disable_because_related_active_job_is_working" => "Location cannot be disabled because there are applicants who have pending jobs to clock out",
                "disable_location_failure" => "Disable location failure",
                "hquser_has_not_comapny" => "HQ user has not company",
                "hquser_is_not_allow" => "HQ user is not allow",
                "hquser_not_exist" => "HQ user is not exist",
                "hquser_not_found" => "HQ user not found",
                "location_has_assign" => "Location has assign for other area user",
                "location_is_disable" => "Location is disabled",
                "location_not_found" => "Location has not been found"
            ],
            "success" => [
                "disable" => "Disable location successfully",
                "enable" => "Enable location successfully",
                "index" => "Retrieved successfully",
                "list" => "Retrieved successfully",
                "show" => "Retrieved successfully",
                "store" => "Created location successfully",
                "summary" => "Retrieved successfully",
                "update" => "Updated location successfully"
            ]
        ],
        "notification_templates" => [
            "success" => [
                "index" => "Retrieved successfully",
                "update" => "{1} :value out of :count notification have been updated|[2,*] :value out of :count notification have been updated"
            ]
        ],
        "notifications" => [
            "success" => [
                "index" => "Retrieved successfully",
                "readAllNotification" => "Make all notification read successfully",
                "readNotification" => "Read notification successfully"
            ]
        ],
        "payments" => [
            "errors" => [
                "applicant_must_complete_feedback_before_the_payment_is_processed" => "Applicant must complete feedback before the payment is processed",
                "current_user_is_not_supper_admin" => "Current user is not supper admin",
                "no_data" => "No data for payment",
                "one_of_the_payments_is_invalid" => "Cannot process payments because there are payments belong to deleted applicant, or belong to applicant has pending feedback, or have no bank account info",
                "payment_belong_to_deleted_applicant_account" => "The applicant account of this payment had been deleted",
                "payment_can_not_processed" => "Payment cannot processed",
                "payment_has_adjustment" => "Payment has adjustment",
                "payment_not_found" => "Payment not found",
                "process_payment_fail" => "Process payment fail"
            ],
            "success" => [
                "getConsumedCredits" => "Get consumed credits successfully",
                "index" => "Retrieved successfully",
                "list" => "Retrieved successfully",
                "processMultiplePaymentWithNoAdjustment" => "Process multiple payment successfully",
                "processedPayment" => "Processed payment successfully",
                "show" => "Retrieved successfully",
                "storeAdjustmentPaymentByAdmin" => "Store adjustment payment successfully",
                "summary" => "Retrieved successfully"
            ]
        ],
        "roles" => [
            "errors" => [
                "feature_cannot_assign_hq" => "Feature :featureName cannot assign for HQ",
                "feature_does_not_exist_or_not_available" => "Feature with id :featureId does not exist or not available for :featureType roles",
                "rol_super_admin_not_allow_modified" => "Role Super Admin not allow modified",
                "role_already_exists" => "Role with name :value already exists",
                "role_name_not_allow_change_name_or_description" => "Role :roleName not allow to change name or description",
                "role_name_not_allow_disabled" => "Role :roleName not allow disabled",
                "role_not_found" => "Role not found",
                "something_went_wrong" => "Something went wrong"
            ],
            "success" => [
                "disable" => "Disabled role successfully",
                "enable" => "Enabled role successfully",
                "getListInternalRoles" => "Retrieved successfully",
                "getListOptionInternalRoles" => "Retrieved successfully",
                "getListPartnerRoles" => "Retrieved successfully",
                "getListRoles" => "Retrieved successfully",
                "getRoleDetail" => "Retrieved successfully",
                "index" => "Retrieved successfully",
                "store" => "Created role successfully",
                "update" => "Updated role successfully"
            ],
            "validate_message" => [
                "permissions_min" => "Please select at least one permission for this role",
                "permissions_present" => "The permissions field is required"
            ]
        ],
        "statistics" => [
            "errors" => [
                "file_not_found" => "File not found"
            ],
            "success" => [
                "upload_file" => "Upload successfully"
            ]
        ],
        "users" => [
            "errors" => [
                "change_password_fail" => "Change password fail",
                "file_not_found" => "File not found",
                "old_password_does_not_match" => "The old password is incorrect",
                "user_not_found" => "User not found"
            ],
            "success" => [
                "adminLogoUp" => "Upload successfully",
                "changePassword" => "Updated password successfully",
                "getAdminLogo" => "Retrieved successfully",
                "markInvalidIdentityForApplicant" => "Mark the applicant is invalid identity successfully",
                "markValidIdentityForApplicant" => "Mark the applicant is valid identity successfully",
                "profileUpdate" => "Updated profile successfully"
            ]
        ]
    ],
    "users" => [
        "errors" => [
            "account_dont_exist" => "Account don't exist",
            "register" => "Register failed",
            "resend_moretimes_than_allowed" => "You have requested resending OTP 5 times. You have to wait for 1 hour to request again",
            "resend_overtime_allowed" => "You have to wait for 5 minutes to request resending OTP",
            "verify_email" => "Verify email fail"
        ],
        "success" => [
            "changePassword" => "Updated password successfully",
            "forgot" => "Send mail success",
            "logout" => "Successfully signed out",
            "register" => "Register successfully",
            "sendOtp" => "The OTP send to your phone number",
            "setSettings" => "Updated successfully",
            "settings" => "Retrieved successfully",
            "show" => "Retrieved successfully",
            "switchLang" => "Change langguage successfully",
            "update" => "Updated successfully",
            "updateCredential" => "Your NRIC/FIN and bank details has been updated so you can start applying for the job now",
            "verifyContact" => "Phone number verified",
            "verify_email" => "Verify email successfully"
        ]
    ],
    "work_experiences" => [
        "success" => [
            "update" => "Updated successfully"
        ]
    ]
];
