<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('question', function (Blueprint $table) {
            $table->id();
            $table->integer('no')->nullable();
            $table->integer('topic_id')->nullable()->unsigned();
            $table->string('title')->nullable();
            $table->string('word')->nullable();
            $table->string('pronunciation_word')->nullable();
            $table->string('meaning')->nullable();
            $table->string('chinese_example')->nullable();
            $table->string('pinyin')->nullable();
            $table->string('meaning_in_vietnamese')->nullable();
            $table->json('incorrect_answers')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('question');
    }
};
