@extends('layouts.app', [
    'namePage' => 'Dashboard',
    'class' => 'login-page sidebar-mini ',
    'activePage' => 'home',
    'backgroundImage' => asset('now') . '/img/bg14.jpg',
])

@section('content')
    <div class="container">
        <div class="title" style="text-align: center;margin-top: 30px">
            <h2>Tạo bộ câu</h2>
        </div>
        <div class="form-create-question">
            <form action="{{ route('create') }}" method="POST">
                @csrf
                <div class="topic">
                    <div class="choice-topic">
                        <label for="topic">Chọn chủ đề:</label>
                        <select name="topic_id" id="topic" required>

                            @if ($topics == '')
                                <option value="">---Hiện không có chủ đề---</option>
                            @else
                                <option value="">---Lựa chọn chủ đề---</option>
                                @foreach ($topics as $topic)
                                    <option value="{{ $topic->id }}">{{ $topic->name }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <a href="{{ route('view.topic') }}" class="btn btn-primary">Tạo mới chủ đề</a>
                </div>
                <div class="form-question">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="input-group mb-3">
                                <input type="text" class="form-control" required placeholder="Word" id="word"
                                    name="word">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="input-group mb-3">
                                <input type="text" class="form-control" required placeholder="Pronunciation Word"
                                    id="pronunciation_word" name="pronunciation_word">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="input-group mb-3">
                                <input type="text" class="form-control" required placeholder="Example" id="example"
                                    name="example">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="input-group mb-3">
                                <input type="text" class="form-control" required placeholder="Example Meaning"
                                    id="exampleMeaning" name="exampleMeaning">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="input-group mb-3">
                                <input type="text" class="form-control" required placeholder="Pronunciation Example"
                                    id="pronunciationExample" name="pronunciationExample">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="input-group mb-3">
                                <input type="text" class="form-control" required placeholder="Correct Answer"
                                    id="correctAnswer" name="correctAnswer">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="input-group mb-3">
                                <input type="text" class="form-control" required placeholder="Incorrect Answersr"
                                    id="incorrectAnswers" name="incorrectAnswers">
                            </div>
                        </div>
                    </div>
                    <div style="text-align: center;margin-top: 40px">
                        <button type="submit" class="btn btn-primary">Tạo ngay</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="display-question">
            <table class="table">
                <thead>
                    <tr>
                        <th>STT</th>
                        <th>Từ</th>
                        <th>Chủ đề</th>
                        <th>Phát âm</th>
                        <th>Ví dụ</th>
                        <th>Nghĩa ví dụ</th>
                        <th>Phát âm ví dụ</th>
                        <th>Đáp án đúng</th>
                        <th>Đáp án sai</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($questions as $question)
                        <tr>
                            <td>{{ $question->id }}</td>
                            <td>{{ $question->title}}</td>
                            <td>{{ $question->word }}</td>
                            <td>{{ $question->pronunciation_word }}</td>
                            <td>{{ $question->meaning }}</td>
                            <td>{{ $question->chinese_example }}</td>
                            <td>{{ $question->pinyin }}</td>
                            <td>{{ $question->meaning_in_vietnamese }}</td>
                            <td>{{ $question->incorrectAnswers }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="ex-im">
                <a href="{{ route('export.data') }}"><button type="" class="btn btn-primary">Export
                        File</button></a>
                <a href="{{ route('addQuestion') }}"><button type="" class="btn btn-primary">Import
                        File</button></a>
            </div>
        </div>
    </div>
</body>

</html>
