@extends('layouts.master')

@section('content')
    <main class="main-content position-relative max-height-vh-100 h-100 mt-1 border-radius-lg ">
        <div class="container-fluid py-4">
            <div class="row">
                <div class="col-12">
                    <div class="card mb-4">
                        <div class="card-header pb-0" style="display: flex;justify-content: space-between">
                            <span>Bộ Câu Hỏi</span>
                            <a href="{{ route('create.question') }}"><button type="" class="btn btn-primary">Tạo mới câu
                                    hỏi
                                </button></a>
                        </div>
                        <div class="choices" data-type="select-one" tabindex="0" role="listbox" aria-haspopup="true"
                            aria-expanded="false">
                            <div class="choices__inner">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <form method="get" action="{{ route('filter') }}">
                                            @csrf
                                            <div class="row" style="margin-left: 10px">
                                                <div class="col-7"><select name="topic_id" id="topic"
                                                        class="form-control choices__input">
                                                        @if (session('topic_id'))
                                                            @foreach ($topics as $topic)
                                                                @if ($topic->id == session('topic_id'))
                                                                    <option value="{{ $topic->id }}" id="filter"
                                                                        selected>
                                                                        {{ $topic->name }}
                                                                    </option>
                                                                @else
                                                                    <option value="{{ $topic->id }}" id="filter">
                                                                        {{ $topic->name }}
                                                                    </option>
                                                                @endif
                                                            @endforeach
                                                        @else
                                                            @if ($topics->isEmpty())
                                                                <option value="0">---Hiện không có chủ đề---</option>
                                                            @else
                                                                @foreach ($topics as $topic)
                                                                    <option value="{{ $topic->id }}" id="filter">
                                                                        {{ $topic->name }}
                                                                    </option>
                                                                @endforeach
                                                            @endif
                                                        @endif
                                                    </select></div>
                                                <div class="col-5" style="display: none">
                                                    <button type="submit" class="btn">Tìm</button>
                                                </div>
                                            </div>

                                        </form>
                                    </div>
                                    <div class="col-sm-8 ex-im" style="text-align: end;padding-right: 35px">
                                        <a href="{{ route('export.data') }}"
                                            @if (session('topic_id') == 0) style="pointer-events: none;opacity: 0.6;" @endif><button
                                                type="" class="btn btn-primary">Export
                                                File</button></a>
                                        <button id="showImportPopup1" class="btn btn-primary"
                                            @if (session('topic_id') == 0) style="pointer-events: none;opacity: 0.6;" @endif>Import
                                            File</button>
                                        <div id="importPopup1" class="popup" style="z-index: 9999">
                                            <div class="popup-content">
                                                <form action="{{ route('import.data') }}" method="POST"
                                                    class="multisteps-form__form mb-8" enctype="multipart/form-data">
                                                    <div style="text-align: right">
                                                        <button id="closePopup1"
                                                            class="btn bg-gradient-dark ms-auto mb-0 js-btn-next"
                                                            style="width: 40px; height: 40px; border-radius: 50%;"><i
                                                                class="fa fa-close" style="margin-left: -4px"></i></button>
                                                    </div>
                                                    <div class="card multisteps-form__panel p-3 border-radius-xl bg-white js-active"
                                                        data-animation="FadeIn">
                                                        <h5 class="font-weight-bolder">Import dữ liệu câu hỏi</h5>
                                                        <div class="multisteps-form__content">
                                                            @csrf
                                                            <div class="col-12 col-sm-12">
                                                                <label for="">Chọn file cần import</label>
                                                                <input type="hidden" name="topic_id"
                                                                    value="{{ session('topic_id') }}">
                                                                <input type="file" class="form-control"
                                                                    placeholder="Tên File" id="file" name="file"
                                                                    class="multisteps-form__input form-control" required>
                                                            </div>
                                                            <div class="button-row d-flex mt-4">
                                                                <button type="submit"
                                                                    class="btn bg-gradient-dark ms-auto mb-0 js-btn-next">Tạo</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                        <button id="showImportPopup2" class="btn btn-primary"
                                            @if (session('topic_id') == 0) style="pointer-events: none;opacity: 0.6;" @endif>Import
                                            File All</button>

                                        <div id="importPopup2" class="popup" style="z-index: 9999">
                                            <div class="popup-content">
                                                <form action="{{ route('import.all.data') }}" method="POST"
                                                    class="multisteps-form__form mb-8" enctype="multipart/form-data">
                                                    <div style="text-align: right">
                                                        <button id="closePopup2"
                                                            class="btn bg-gradient-dark ms-auto mb-0 js-btn-next"
                                                            style="width: 40px; height: 40px; border-radius: 50%;"><i
                                                                class="fa fa-close" style="margin-left: -4px"></i></button>
                                                    </div>
                                                    <div class="card multisteps-form__panel p-3 border-radius-xl bg-white js-active"
                                                        data-animation="FadeIn">
                                                        <h5 class="font-weight-bolder">Import lại toàn bộ câu hỏi của chủ đề
                                                        </h5>
                                                        <div class="multisteps-form__content">
                                                            @csrf
                                                            <div class="col-12 col-sm-12">
                                                                <label for="">Chọn file cần import</label>
                                                                <input type="hidden" name="topic_id"
                                                                    value="{{ session('topic_id') }}">
                                                                <input type="file" class="form-control"
                                                                    placeholder="Tên File" id="file" name="file"
                                                                    class="multisteps-form__input form-control" required>
                                                            </div>
                                                            <div class="button-row d-flex mt-4">
                                                                <button type="submit"
                                                                    class="btn bg-gradient-dark ms-auto mb-0 js-btn-next">Tạo</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                        {{-- <a href="{{ route('addQuestion') }}"><button type=""
                                                class="btn btn-primary">Import
                                                File</button></a> --}}
                                    </div>
                                    @if (session()->has('success'))
                                        <div x-data="{ show: true }" x-init="setTimeout(() => show = false, 3000)" x-show="show">
                                            <div class="alert alert-success">
                                                {{ session('success') }}
                                            </div>
                                        </div>
                                    @endif

                                    @if (session()->has('error'))
                                        <div x-data="{ show: true }" x-init="setTimeout(() => show = false, 3000)" x-show="show">
                                            <div class="alert alert-warning">
                                                {{ session('error') }}
                                            </div>
                                        </div>
                                    @endif
                                </div>

                            </div>
                        </div>
                        <div class="card-body px-0 pt-0 pb-2">
                            <div class="table-responsive p-0">
                                <div class="display-question">
                                    <table class="table">
                                        <thead>
                                            <th
                                                class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                                No</th>
                                            <th
                                                class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                                Title</th>
                                            <th
                                                class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                                New Words</th>
                                            <th
                                                class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                                Pronunciation</th>
                                            <th
                                                class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                                Meaning</th>
                                            <th
                                                class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                                Chinese Example</th>
                                            <th
                                                class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                                Pinyin</th>
                                            <th
                                                class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                                Meaning in Vietnamese</th>
                                            {{-- <th
                                                class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                                Incorrect Answers</th> --}}
                                            <th
                                                class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                                Action</th>
                                        </thead>
                                        <div id="question-body">
                                            <tbody>
                                                @foreach ($questions as $question)
                                                    <tr>
                                                        <td class="align-middle text-center">
                                                            <span
                                                                class="text-secondary text-xs font-weight-bold">{{ $question->no }}</span>
                                                        </td>
                                                        <td class="align-middle text-center">
                                                            <span
                                                                class="text-secondary text-xs font-weight-bold">{{ $question->title }}</span>
                                                        </td>
                                                        <td class="align-middle text-center">
                                                            <span class="text-secondary text-xs font-weight-bold"
                                                                class="topic_name">{{ $question->word }}</span>
                                                        </td>
                                                        <td class="align-middle text-center">
                                                            <span
                                                                class="text-secondary text-xs font-weight-bold">{{ $question->pronunciation_word }}</span>
                                                        </td>
                                                        <td class="align-middle text-center">
                                                            <span
                                                                class="text-secondary text-xs font-weight-bold">{{ $question->meaning }}</span>
                                                        </td>
                                                        <td class="align-middle text-center">
                                                            <span
                                                                class="text-secondary text-xs font-weight-bold">{{ $question->chinese_example }}</span>
                                                        </td>
                                                        <td class="align-middle text-center">
                                                            <span
                                                                class="text-secondary text-xs font-weight-bold">{{ $question->pinyin }}</span>
                                                        </td>
                                                        <td class="align-middle text-center">
                                                            <span
                                                                class="text-secondary text-xs font-weight-bold">{{ $question->meaning_in_vietnamese }}</span>
                                                        </td>
                                                        {{-- <td class="align-middle text-center">
                                                            <span
                                                                class="text-secondary text-xs font-weight-bold">{{ $question->incorrect_answers }}</span>
                                                        </td> --}}
                                                        <td class="align-middle text-center">
                                                            <div class="row">
                                                                <div class="col-sm-6"><a
                                                                        href="{{ url('/question/edit', $question->id) }}"><button
                                                                            class="btn btn-wraning">{{ trans('Edit') }}</button></a>

                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <form method="post" class="delete_form"
                                                                        action="{{ url('/question', $question->id) }}"
                                                                        id="{{ $question->id }}">
                                                                        {{ method_field('GET') }}
                                                                        {{ csrf_field() }}
                                                                        <button type="submit"
                                                                            class="btn btn-danger">{{ trans('Delete') }}</button>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </div>
                                    </table>
                                    <div style="text-align: center">{{ $questions->links('pagination.custom') }}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <script>
        $(document).ready(function() {
            $('#topic').on('change', function() {
                var selectedValue = $(this).val();
                window.location.href = '/filter/question?topic_id=' + selectedValue;
            });
        });
        document.getElementById('showImportPopup1').addEventListener('click', function() {
            document.getElementById('importPopup1').style.display = 'block';
        });

        document.getElementById('closePopup1').addEventListener('click', function() {
            document.getElementById('importPopup1').style.display = 'none';
        });

        document.getElementById('showImportPopup2').addEventListener('click', function() {
            document.getElementById('importPopup2').style.display = 'block';
        });

        document.getElementById('closePopup2').addEventListener('click', function() {
            document.getElementById('importPopup2').style.display = 'none';
        });
    </script>
    <script>
        $(document).ready(function() {
            $('.delete_form').on('submit', function() {
                if (confirm("Are you sure you want to delete it?")) {
                    return true;
                } else {
                    return false;
                }
            });
        });
    </script>
@endsection
