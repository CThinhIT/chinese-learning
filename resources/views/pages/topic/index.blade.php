@extends('layouts.master')

@section('content')
    <main class="main-content position-relative max-height-vh-100 h-100 mt-1 border-radius-lg ">
        <div class="container-fluid py-4">
            <div class="row">
                <div class="col-12">
                    <div class="card mb-4">
                        <div class="card-header pb-0" style="display: flex;justify-content: space-between">
                            <span>Các chủ đề</span>
                            <a href="{{ route('view.topic') }}"><button type="" class="btn btn-primary">Tạo mới chủ đề
                                </button></a>
                        </div>

                        <div class="card-body px-0 pt-0 pb-2">
                            <div class="table-responsive p-0">
                                <div class="display-question">
                                    <table class="table">
                                        <thead>
                                            <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                                No</th>
                                            <th
                                                class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                                Name Topic</th>
                                            <th
                                                class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                                Action</th>
                                        </thead>
                                        <tbody>
                                            @foreach ($topics as $topic)
                                                <tr>
                                                    <td class="align-middle text-center">
                                                        <span
                                                            class="text-secondary text-xs font-weight-bold">{{ $topic->id }}</span>
                                                    </td>
                                                    <td class="align-middle text-center">
                                                        <span
                                                            class="text-secondary text-xs font-weight-bold">{{ $topic->name }}</span>
                                                    </td>
                                                    <td class="align-middle text-center">
                                                        <a href="{{ url('/topic/edit/', $topic->id) }}"></a>
                                                        <button class="btn btn-danger">{{ trans('Edit') }}</button>
                                                        <form method="post" class="delete_form"
                                                            action="{{ url('/topic', $topic->id) }}"
                                                            id="{{ $topic->id }}">
                                                            {{ method_field('GET') }}
                                                            {{ csrf_field() }}
                                                            <button type="submit"
                                                                class="btn btn-danger">{{ trans('Delete') }}</button>
                                                        </form>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <script>
        $(document).ready(function() {
            $('.delete_form').on('submit', function() {
                if (confirm("Are you sure you want to delete it?")) {
                    return true;
                } else {
                    return false;
                }
            });
        });
    </script>
@endsection
