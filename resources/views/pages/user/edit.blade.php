@extends('layouts.master')

@section('content')
    <main class="main-content position-relative max-height-vh-100 h-100 mt-1 border-radius-lg ">
        <div class="container-fluid py-4">
            <div class="row">
                <div class="col-12 col-lg-8 m-auto">
                    <div class="card mb-4">
                        <div class="card-body px-0 pt-0 pb-2">
                            <div class="table-responsive p-0">
                                <form action="{{ route('user.update') }}" method="POST" class="multisteps-form__form mb-8">
                                    <div class="card multisteps-form__panel p-3 border-radius-xl bg-white js-active"
                                        data-animation="FadeIn">
                                        <h5 class="font-weight-bolder">Detail Topic</h5>
                                        <div class="multisteps-form__content">
                                            @csrf
                                            <div class="row">
                                                <div class="col-md-4 pr-1">
                                                    <div class="form-group">
                                                        <label>{{ __(' First Name') }}</label>
                                                        <input type="text" name="fName" class="form-control"
                                                            value="{{ old('name', $users->first_name) }}">
                                                    </div>
                                                </div>
                                                <div class="col-md-4 pr-1">
                                                    <div class="form-group">
                                                        <label>{{ __(' Last Name') }}</label>
                                                        <input type="text" name="lName" class="form-control"
                                                            value="{{ old('name', $users->last_name) }}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-8 pr-1">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1">{{ __(' Email address') }}</label>
                                                        <input type="email" name="email" class="form-control"
                                                            placeholder="Email"
                                                            value="{{ old('email', $users->email) }}">
                                                    </div>
                                                </div>
                                            </div>
                                            @if (session('message'))
                                                <p class="text-success"> {{ session('message') }}</p>
                                            @endif
                                            <div class="card-footer ">
                                                <button type="submit"
                                                    class="btn btn-primary btn-round">{{ __('Save') }}</button>
                                            </div>
                                            <hr class="half-rule" />
                                        </div>
                                    </div>
                                </form>
                            </div>


                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-8 m-auto">
                    <div class="card mb-4">
                        <div class="card-body px-0 pt-0 pb-2">
                            <div class="table-responsive p-0">
                                <form method="post" action="{{ route('change.password') }}" autocomplete="off">
                                    @csrf
                                    <div class="row">
                                        <div class="col-md-8 pr-1">
                                            <div class="form-group {{ $errors->has('password') ? ' has-danger' : '' }}">
                                                <label>{{ __(' Current Password') }}</label>
                                                <input
                                                    class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}"
                                                    name="old_password" placeholder="{{ __('Current Password') }}"
                                                    type="password" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-8 pr-1">
                                            <div class="form-group {{ $errors->has('password') ? ' has-danger' : '' }}">
                                                <label>{{ __(' New password') }}</label>
                                                <input
                                                    class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}"
                                                    placeholder="{{ __('New Password') }}" type="password" name="password"
                                                    required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-8 pr-1">
                                            <div class="form-group {{ $errors->has('password') ? ' has-danger' : '' }}">
                                                <label>{{ __(' Confirm New Password') }}</label>
                                                <input class="form-control" placeholder="{{ __('Confirm New Password') }}"
                                                    type="password" name="repassword" required>
                                            </div>
                                        </div>
                                    </div>
                                    @if (session('message-rs'))
                                        <p class="text-success"> {{ session('message-rs') }}</p>
                                    @endif
                                    <div class="card-footer ">
                                        <button type="submit"
                                            class="btn btn-primary btn-round ">{{ __('Change Password') }}</button>
                                    </div>
                                </form>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
