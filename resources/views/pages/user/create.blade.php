@extends('layouts.master')

@section('content')
    <main class="main-content position-relative max-height-vh-100 h-100 mt-1 border-radius-lg ">
        <div class="container-fluid py-4">
            <div class="row">
                <div class="col-12 col-lg-8 m-auto">
                    <div class="card mb-4">
                        <div class="card-body px-0 pt-0 pb-2">
                            <div class="table-responsive p-0">
                                <form action="{{ route('user.create') }}" method="POST"
                                    class="multisteps-form__form mb-8">
                                    <div class="card multisteps-form__panel p-3 border-radius-xl bg-white js-active"
                                        data-animation="FadeIn">
                                        <h5 class="font-weight-bolder">Tạo người dùng</h5>
                                        <div class="multisteps-form__content">
                                            @csrf
                                            <div class="col-12 col-sm-12">
                                                <label for="">Tên chủ đề</label>
                                                <input type="text" class="form-control" placeholder="Name Topic"
                                                    id="name" name="name"
                                                    class="multisteps-form__input form-control" required>
                                            </div>
                                            <div class="button-row d-flex mt-4">
                                                <button type="submit"
                                                    class="btn bg-gradient-dark ms-auto mb-0 js-btn-next">Tạo</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
