@extends('layouts.master')

@section('content')
    <main class="main-content position-relative max-height-vh-100 h-100 mt-1 border-radius-lg ">
        <div class="container-fluid py-4">
            <div class="row">
                <div class="col-12">
                    <div class="card mb-4">
                        <div class="card-header pb-0" style="display: flex;justify-content: space-between">
                            <span>Danh sách người dùng</span>
                            <a href="{{ route('view.topic') }}"><button type="" class="btn btn-primary">Tạo người dùng
                                    mới
                                </button></a>
                        </div>

                        <div class="card-body px-0 pt-0 pb-2">
                            <div class="table-responsive p-0">
                                <div class="display-question">
                                    <table class="table">
                                        <thead>
                                            <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                                No</th>
                                            <th
                                                class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                                User Name</th>
                                            <th
                                                class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                                Email</th>
                                            <th
                                                class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                                Action</th>
                                        </thead>
                                        <tbody>
                                            @foreach ($users as $user)
                                                <tr>
                                                    <td class="align-middle text-center">
                                                        <span
                                                            class="text-secondary text-xs font-weight-bold">{{ $user->id }}</span>
                                                    </td>
                                                    <td class="align-middle text-center">
                                                        <span
                                                            class="text-secondary text-xs font-weight-bold">{{ $user->name }}</span>
                                                    </td>
                                                    <td class="align-middle text-center">
                                                        <span
                                                            class="text-secondary text-xs font-weight-bold">{{ $user->email }}</span>
                                                    </td>
                                                    <td class="align-middle text-center">
                                                        <div class="row">
                                                            <div class="col-sm-6"><a
                                                                    href="{{ url('/user/edit', $user->id) }}"><button
                                                                        class="btn btn-wraning">{{ trans('Edit') }}</button></a>

                                                            </div>
                                                            <div class="col-sm-6">
                                                                <form method="post" class="delete_form"
                                                                    action="{{ url('/user', $user->id) }}"
                                                                    id="{{ $user->id }}">
                                                                    {{ method_field('GET') }}
                                                                    {{ csrf_field() }}
                                                                    <button type="submit"
                                                                        class="btn btn-danger">{{ trans('Delete') }}</button>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <script>
        $(document).ready(function() {
            $('.delete_form').on('submit', function() {
                if (confirm("Are you sure you want to delete it?")) {
                    return true;
                } else {
                    return false;
                }
            });
        });
    </script>
@endsection
