@extends('layouts.master')

@section('content')
    <main class="main-content position-relative max-height-vh-100 h-100 mt-1 border-radius-lg ">
        <div class="container-fluid py-4">
            <div class="row">
                <div class="col-12 col-lg-8 m-auto">
                    <div class="card mb-4">
                        <div class="card-body px-0 pt-0 pb-2">
                            <div class="table-responsive p-0">
                                <form action="{{ route('create') }}" method="POST" class="multisteps-form__form mb-8">
                                    <div class="card multisteps-form__panel p-3 border-radius-xl bg-white js-active"
                                        data-animation="FadeIn">
                                        <h5 class="font-weight-bolder">Tạo bộ từ mới</h5>
                                        <div class="multisteps-form__content row">
                                            @csrf
                                            <div class="col-12 col-sm-6">
                                                <label for="">Word</label>
                                                <input type="text" class="form-control" placeholder="Word" id="word"
                                                    name="word" class="multisteps-form__input form-control" required>
                                            </div>
                                            <div class="col-12 col-sm-6">
                                                <label for="">Title</label>
                                                <input type="text" class="form-control" placeholder="Title"
                                                    id="title" name="title"
                                                    class="multisteps-form__input form-control" required>
                                            </div>
                                            <div class="col-12 col-sm-6">
                                                <label for="">Pronunciation Word</label>
                                                <input type="text" class="form-control" placeholder="Pronunciation Word<"
                                                    id="pronunciationWord" name="pronunciationWord"
                                                    class="multisteps-form__input form-control" required>
                                            </div>
                                            <div class="col-12 col-sm-6">
                                                <label for="">Meaning</label>
                                                <input type="text" class="form-control" placeholder="Meaning"
                                                    id="meaning" name="meaning"
                                                    class="multisteps-form__input form-control" required>
                                            </div>
                                            <div class="col-12 col-sm-6">
                                                <label for="">Chinese Example</label>
                                                <input type="text" class="form-control" placeholder="Chinese Example"
                                                    id="chinese_example" name="chinese_example"
                                                    class="multisteps-form__input form-control" required>
                                            </div>
                                            <div class="col-12 col-sm-6">
                                                <label for="">Pinyin</label>
                                                <input type="text" class="form-control" placeholder="Pinyin"
                                                    id="pinyin" name="pinyin"
                                                    class="multisteps-form__input form-control" required>
                                            </div>
                                            <div class="col-12 col-sm-6">
                                                <label for="">Meaning in Vietnamese</label>
                                                <input type="text" class="form-control"
                                                    placeholder="Meaning in Vietnamese" id="meaning_in_vietnamese"
                                                    name="meaning_in_vietnamese" class="multisteps-form__input form-control"
                                                    required>
                                            </div>
                                            <div class="col-12 col-sm-6">
                                                <label for="">Incorrect Answers</label>
                                                <input type="text" class="form-control" placeholder="Incorrect Answers"
                                                    id="incorrectAnswers" name="incorrectAnswers"
                                                    class="multisteps-form__input form-control" required>
                                            </div>
                                            <input type="hidden" name="topic_id" value="{{ session('topic_id') }}">

                                            <div style="text-align: center;margin-top: 40px">
                                                <button type="submit" class="btn btn-primary">Tạo ngay</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
