<?php

use App\Http\Controllers\Apis\RegisterController;
use App\Http\Controllers\Apis\LoginController;
use App\Http\Controllers\Apis\QuestionController;
use App\Http\Controllers\Apis\TopicController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::namespace('api')->group(function () {

    Route::post('/sign-up', [RegisterController::class, 'register'])->name('register');


    Route::prefix('auth')->group(function () {
        Route::post('/login', [LoginController::class, 'login'])->name('login');
        // Route::post('/forgot-password', [AuthController::class, 'forgot']);
        // Route::post('/reset-password', [AuthController::class, 'resetPassword']);
        // Route::post('/check-reset-password-token', [AuthController::class, 'checkResetPasswordTokenValidity']);
        // Route::group([
        //     'middleware' => ['auth:api']
        // ],
        // function () {
        //     Route::post('/add-device-token', [AuthController::class, 'addDeviceToken']);
        //     Route::post('/logout', [AuthController::class, 'logout']);
        //     Route::post('/refresh', [AuthController::class, 'refresh']);
        //     Route::get('/user-profile', [AuthController::class, 'userProfile']);
        //     Route::get('/admin-location-profile', [AuthController::class, 'locationManagerProfile']);
        // });
    });
    Route::prefix('topic')->group(function () {
        Route::get('/list-topics', [TopicController::class, 'listTopic']);
    });
    Route::prefix('question')->group(function () {
        Route::get('/list-questions', [QuestionController::class, 'listQuestion']);
    });
});
