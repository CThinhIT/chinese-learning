<?php

use App\Http\Controllers\QuestionController;
use App\Http\Controllers\TopicController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [QuestionController::class, 'index'])->name('home');

Route::get('dashboard', function () {
    return view('dashboard');
})->name('dashboard');

Route::post('/create', [QuestionController::class, 'store'])->name('create');

Route::get('chinese/export', [QuestionController::class, 'export'])->name('export.data');

Route::post('chinese/import', [QuestionController::class, 'import'])->name('import.data');


Route::post('chinese/importF', [QuestionController::class, 'importFull'])->name('import.all.data');

Route::post('/create/topic', [QuestionController::class, 'createTopic'])->name('create_topic');

Route::get('/view/topic', [TopicController::class, 'store'])->name('view.topic');


Route::get('dashboard', function () {
    return view('dashboard');
})->name('dashboard');

Route::get('billing', function () {
    return view('billing');
})->name('billing');

Route::get('profile', function () {
    return view('profile');
})->name('profile');

Route::get('rtl', function () {
    return view('rtl');
})->name('rtl');

Route::get('user-management', function () {
    return view('laravel-examples/user-management');
})->name('user-management');

Route::get('/', [QuestionController::class, 'index'])->name('home');

Route::get('question', [QuestionController::class, 'index'])->name('question');

Route::get('topic', [TopicController::class, 'index'])->name('topic');

Route::get('create/question', [QuestionController::class, 'viewCreateQuestion'])->name('create.question');

Route::get('virtual-reality', function () {
    return view('virtual-reality');
})->name('virtual-reality');

Route::get('static-sign-in', function () {
    return view('static-sign-in');
})->name('sign-in');

Route::get('static-sign-up', function () {
    return view('static-sign-up');
})->name('sign-up');

Route::get('/login', function () {
    return view('dashboard');
})->name('sign-up');
Route::get('/filter/question', [QuestionController::class, 'filter'])->name('filter');

Route::get('/topic/{id}', [TopicController::class, 'delete'])->name('topic.delete');
Route::get('/topic/edit/{id}', [TopicController::class, 'update']);
Route::post('/topic/edit', [TopicController::class, 'updatePost'])->name('topic.update');

Route::get('/question/{id}', [QuestionController::class, 'delete'])->name('question.delete');
Route::get('/question/edit/{id}', [QuestionController::class, 'update']);
Route::post('/question/edit', [QuestionController::class, 'updatePost'])->name('question.update');

Route::prefix('user')->group(function () {
    Route::get('/', [UserController::class, 'index']);
    Route::get('/edit/{id}', [UserController::class, 'update']);
    Route::post('/edit', [UserController::class, 'updatePost'])->name('user.update');
    Route::post('/create', [UserController::class, 'store'])->name('user.create');
    Route::post('/create', [UserController::class, 'store'])->name('change.password');
});
